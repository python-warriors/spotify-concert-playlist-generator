.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Spotify Concert Playlist Generator, run this command in your terminal:

.. code-block:: console

    $ pip install spotify_concert_playlist_generator

This is the preferred method to install Spotify Concert Playlist Generator, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for Spotify Concert Playlist Generator can be downloaded from the `Github repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://github.com/sfauch1/spotify_concert_playlist_generator

Or download the `tarball`_:

.. code-block:: console

    $ curl -OJL https://github.com/sfauch1/spotify_concert_playlist_generator/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Github repo: https://github.com/sfauch1/spotify_concert_playlist_generator
.. _tarball: https://github.com/sfauch1/spotify_concert_playlist_generator/tarball/master
