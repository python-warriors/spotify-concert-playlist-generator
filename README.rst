==================================
Spotify Concert Playlist Generator
==================================


.. image:: https://img.shields.io/pypi/v/spotify_concert_playlist_generator.svg
        :target: https://pypi.python.org/pypi/spotify_concert_playlist_generator

.. image:: https://img.shields.io/travis/sfauch1/spotify_concert_playlist_generator.svg
        :target: https://travis-ci.com/sfauch1/spotify_concert_playlist_generator

.. image:: https://readthedocs.org/projects/spotify-concert-playlist-generator/badge/?version=latest
        :target: https://spotify-concert-playlist-generator.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status


.. image:: https://pyup.io/repos/github/sfauch1/spotify_concert_playlist_generator/shield.svg
     :target: https://pyup.io/repos/github/sfauch1/spotify_concert_playlist_generator/
     :alt: Updates



Utility to create a Spotify playlist based on Setlist.fm setlists


* Free software: MIT license
* Documentation: https://spotify-concert-playlist-generator.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
