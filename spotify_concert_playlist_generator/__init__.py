"""Top-level package for Spotify Concert Playlist Generator."""

__author__ = """Stephane Faucher"""
__email__ = 'stephane.faucher@gmail.com'
__version__ = '0.1.0'
